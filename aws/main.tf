terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.27"
    }
  }
  backend "s3" {
    bucket         = "bullwise-tfstate"
    key            = "bullwise.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "bullwise-tfstate-lock"
  }
}

provider "aws" {
  region = var.region_1
}

module "env" {
  source   = "./environments/staging"
  count    = terraform.workspace == "staging" ? 1 : 0
  region_1 = var.region_1
  prefix   = "${var.project}-${terraform.workspace}"
  common_tags = {
    Project     = var.project
    Environment = terraform.workspace,
  }
}


output "api_public_ip" {
  value = module.env[*].api_public_ip
}
