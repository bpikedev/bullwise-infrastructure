module "api_ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "4.1.4"

  name = "${var.prefix}-api-ec2"

  ami           = data.aws_ami.latest_amazon_linux2.id
  instance_type = var.api_instance_type
  monitoring    = true

  vpc_security_group_ids = [module.api_security_group.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  key_name               = var.api_instance_keyname

  user_data = templatefile("${path.root}/${var.api_template_file_path}", {})

  tags = merge(
    var.common_tags,
    { "Service" : "api" }
  )
}

data "template_file" "user_data" {
  template = file("${path.root}/${var.api_template_file_path}")
}

data "aws_ami" "latest_amazon_linux2" {
  # # Dynamically retrieve desired ami
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-x86_64-gp2"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

module "api_security_group" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "${var.prefix}-api-sg"
  description = "API security group"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp"]

  ingress_with_cidr_blocks = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = ""
      cidr_blocks = "0.0.0.0/0"
    },
    {
      from_port   = 8080
      to_port     = 8080
      protocol    = "tcp"
      description = ""
      cidr_blocks = "0.0.0.0/0"
    },
  ]
  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = ""
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

output "api_public_ip" {
  value = module.api_ec2_instance.public_ip
}
