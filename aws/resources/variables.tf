variable "prefix" {}
variable "common_tags" {}
variable "node_env" {}

variable "vpc_1_cidr" {}
variable "vpc_1_azs" {}
variable "vpc_1_subnet_private_1" {}
variable "vpc_1_subnet_public_1" {}

variable "api_instance_type" {}
variable "api_instance_keyname" {}
variable "api_template_file_path" {
  default = "resources/api.yaml"
}
