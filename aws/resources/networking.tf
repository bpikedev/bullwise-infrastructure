module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.prefix}-vpc-1"
  cidr = var.vpc_1_cidr

  azs            = var.vpc_1_azs
  public_subnets = [var.vpc_1_subnet_public_1]

  tags = var.common_tags
}
