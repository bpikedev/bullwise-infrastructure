terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.27"
    }
  }
}

provider "aws" {
  region = var.region_1
}

module "env" {
  source = "./environments/staging"
}

module "resources" {
  source = "./resources"
}
