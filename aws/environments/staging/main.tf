module "resources" {
  source = "../../resources"

  prefix      = var.prefix
  common_tags = var.common_tags
  node_env    = "staging"

  vpc_1_cidr             = "10.0.0.0/16"
  vpc_1_subnet_private_1 = "10.0.1.0/24"
  vpc_1_subnet_public_1  = "10.0.101.0/24"
  vpc_1_azs              = ["${var.region_1}a", "${var.region_1}b", "${var.region_1}c"]

  # API
  api_instance_type    = "t2.micro"
  api_instance_keyname = "${var.prefix}-api"
}

output "api_public_ip" {
  value = module.resources.api_public_ip
}

